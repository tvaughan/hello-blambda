(ns hello-test
  (:require
   [clojure.test :refer [deftest is]]
   [hello :as hello]))

(deftest find-ip-addrs-v4
  (is (= ["127.0.0.1"] (hello/find-ip-addrs-v4 "127.0.0.1")))
  (is (= ["127.0.0.1" "192.168.0.1"] (hello/find-ip-addrs-v4 [42 "127.0.0.1" "192.168.0.1" "256.0.0.1" "10..0.1" "12.34.56.78.42"])))
  (is (= ["127.0.0.1" "192.168.0.1"] (hello/find-ip-addrs-v4 {:one [{:two 42 {:three "127.0.0.1"} {:four "192.168.0.1"}}]}))))
