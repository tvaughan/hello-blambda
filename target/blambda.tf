variable "runtime_layer_name" {}
variable "runtime_layer_compatible_architectures" {}
variable "runtime_layer_compatible_runtimes" {}
variable "runtime_layer_filename" {}

variable "s3_bucket" {}
variable "runtime_layer_s3_key" {}


variable "deps_layer_name" {}
variable "deps_layer_compatible_architectures" {}
variable "deps_layer_compatible_runtimes" {}
variable "deps_layer_filename" {}

variable "deps_layer_s3_key" {}



variable "lambda_name" {}
variable "lambda_handler" {}
variable "lambda_filename" {}
variable "lambda_memory_size" {}
variable "lambda_runtime" {}
variable "lambda_architectures" {}

variable "lambda_s3_key" {}



resource "aws_s3_bucket" "artifacts" {
  bucket = var.s3_bucket
}

resource "aws_s3_object" "lambda" {
  bucket = var.s3_bucket
  key = var.lambda_s3_key
  source = var.lambda_filename
  source_hash = filebase64sha256(var.lambda_filename)
}


module "runtime" {
  source = "./modules"

  layer_name = var.runtime_layer_name
  compatible_architectures = var.runtime_layer_compatible_architectures
  compatible_runtimes = var.runtime_layer_compatible_runtimes
  filename = var.runtime_layer_filename

  s3_bucket = aws_s3_bucket.artifacts.bucket
  s3_key = var.runtime_layer_s3_key

}


module "deps" {
  source = "./modules"

  layer_name = var.deps_layer_name
  compatible_architectures = var.deps_layer_compatible_architectures
  compatible_runtimes = var.deps_layer_compatible_runtimes
  filename = var.deps_layer_filename

  s3_bucket = aws_s3_bucket.artifacts.bucket
  s3_key = var.deps_layer_s3_key

}


resource "aws_lambda_function" "lambda" {
  depends_on = [aws_cloudwatch_log_group.lambda]

  function_name = var.lambda_name

  role = "${aws_iam_role.hello_blambda_iam_role.arn}"

  handler = var.lambda_handler
  memory_size = var.lambda_memory_size
  source_code_hash = filebase64sha256(var.lambda_filename)

  s3_bucket = aws_s3_object.lambda.bucket
  s3_key = aws_s3_object.lambda.key

  runtime = var.lambda_runtime
  architectures = var.lambda_architectures
  layers = [
    module.runtime.arn,

    module.deps.arn,

  ]

}

resource "aws_cloudwatch_log_group" "lambda" {
  name = "/aws/lambda/${var.lambda_name}"
}


