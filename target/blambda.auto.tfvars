runtime_layer_name = "blambda"
runtime_layer_compatible_architectures = [
  
  "arm64",
  
]
runtime_layer_compatible_runtimes = [
  
  "provided.al2",
  
]
runtime_layer_filename = "/mnt/workdir/target/blambda.zip"

s3_bucket = "hello-blambda-artifacts"
runtime_layer_s3_key = "hello-blambda-artifacts/blambda.zip"



deps_layer_name = "hello-blambda-deps"
deps_layer_compatible_architectures = [
  
  "x86_64",
  
  "arm64",
  
]
deps_layer_compatible_runtimes = [
  
  "provided",
  
  "provided.al2",
  
]
deps_layer_filename = "/mnt/workdir/target/hello-blambda-deps.zip"

deps_layer_s3_key = "hello-blambda-artifacts/hello-blambda-deps.zip"


lambda_name = "hello-blambda"
lambda_handler = "hello/handler"
lambda_filename = "/mnt/workdir/target/hello-blambda.zip"
lambda_memory_size = "512"
lambda_runtime = "provided.al2"
lambda_architectures = ["arm64"]

lambda_s3_key = "hello-blambda-artifacts/hello-blambda.zip"

