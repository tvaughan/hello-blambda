terraform {
  backend "s3" {
    bucket = "hello-blambda-remote-state"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}

resource "aws_iam_role" "hello_blambda_iam_role" {
  name = "hello-blambda-lambda"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_policy" "hello_blambda_iam_policy" {
  name = "hello-blambda-lambda"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "s3:GetObject",
          "s3:PutObject"
        ]
        Resource = [
          "${aws_s3_bucket.hello_blambda_s3_bucket.arn}/*"
        ]
      },
      {
        Effect = "Allow"
        Action = [
          "s3:GetObject"
        ]
        Resource = [
          "arn:aws:s3:::logs.tocino.cl/logs/*"
        ]
      },
      {
        Effect = "Allow"
        Action = [
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ]
        Resource = "${aws_cloudwatch_log_group.lambda.arn}:*"
      },
      {
        Effect = "Allow",
        Action = [
          "s3:ListBucket"
        ]
        Resource = [
          "arn:aws:s3:::logs.tocino.cl"
        ]
        Condition = {
          "StringLike" : {
            "s3:prefix" : "logs/*"
          }
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "hello_blambda" {
  role       = aws_iam_role.hello_blambda_iam_role.name
  policy_arn = aws_iam_policy.hello_blambda_iam_policy.arn
}

resource "aws_s3_bucket" "hello_blambda_s3_bucket" {
  bucket = "hello-blambda"
}

resource "aws_lambda_permission" "hello_blambda_lambda_permission" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.hello_blambda_s3_bucket.arn
}

resource "aws_s3_bucket_notification" "hello_blambda_s3_bucket_notification" {
  bucket = aws_s3_bucket.hello_blambda_s3_bucket.id

  depends_on = [aws_lambda_permission.hello_blambda_lambda_permission]

  lambda_function {
    lambda_function_arn = aws_lambda_function.lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "test/"
    filter_suffix       = ".in.json"
  }
}
