# Quick Start

* Requires `podman`

## Set `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` environment variables

TODO: Permissions

## Run linter and tests

```bash
make lint test
```

## Upload test data, trigger lambda, and download results

```bash
make build-lambda deploy-lambda test-lambda
```

* NOTE: If you encounter an error like the one below on the first try, re-run
  the commands above a second time. TODO: Report bug to blambda?

```
╷
│ Error: uploading object to S3 bucket (hello-blambda-artifacts): NoSuchBucket: The specified bucket does not exist
│       status code: 404, request id: FYR7KSVSZ0Q0C2X4, host id: PhVYtkTnPdwZlaaBvRZ5Rpaj+17bUcNn0gPqCqAKhuWQXwD+/AFB0iQ/45anm9hocS5TEDGxXdD2xk9Sy0yryBX6++xKqIh85fUDTiasTT0=
│
│   with aws_s3_object.lambda,
│   on blambda.tf line 34, in resource "aws_s3_object" "lambda":
│   34: resource "aws_s3_object" "lambda" {
│
╵
```
