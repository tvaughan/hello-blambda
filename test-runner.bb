#!/usr/bin/env bb

;; https://book.babashka.org/#_running_tests

(require
 '[babashka.classpath :as cp]
 '[clojure.test :as t])

(cp/add-classpath "src:test")

(require 'hello-test)

(def test-results
  (t/run-tests 'hello-test))

(def failures-and-errors
  (let [{:keys [fail error]} test-results]
    (+ fail error)))

(System/exit failures-and-errors)
