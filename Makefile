# -*- coding: utf-8; mode: makefile-gmake; -*-

MAKEFLAGS += --warn-undefined-variables

SHELL := bash
.SHELLFLAGS := -euo pipefail -c

.PHONY: all
all: lint test

.PHONY: has-command-%
has-command-%:
	@$(if $(shell command -v $* 2> /dev/null),,$(error The command $* does not exist in PATH))

.PHONY: is-defined-%
is-defined-%:
	@$(if $(value $*),,$(error The environment variable $* is undefined))

.PHONY: is-repo-clean
is-repo-clean: has-command-git
	@git diff-index --quiet HEAD --

.PHONY: build-container-image
build-container-image: has-command-podman
	@podman build --pull -f Containerfile -t registry.gitlab.com/tvaughan/hello-blambda:latest

.PHONY: lint
lint: build-container-image is-defined-AWS_ACCESS_KEY_ID is-defined-AWS_SECRET_ACCESS_KEY
	@podman run --rm                                                        \
	    --env AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID)                        \
	    --env AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY)                \
	    --volume $(PWD):/mnt/workdir                                        \
	    --volume hello-blambda-deps:/root                                   \
	    registry.gitlab.com/tvaughan/hello-blambda:latest                   \
	    clj-kondo --lint *.edn src test                                     \
	    #

.PHONY: test
test: build-container-image is-defined-AWS_ACCESS_KEY_ID is-defined-AWS_SECRET_ACCESS_KEY
	@podman run --rm                                                        \
	    --env AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID)                        \
	    --env AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY)                \
	    --volume $(PWD):/mnt/workdir                                        \
	    --volume hello-blambda-deps:/root                                   \
	    registry.gitlab.com/tvaughan/hello-blambda:latest                   \
	    ./test-runner.bb                                                    \
	    #

.PHONY: shell
shell: build-container-image is-defined-AWS_ACCESS_KEY_ID is-defined-AWS_SECRET_ACCESS_KEY
	@podman run --rm -it                                                    \
	    --env AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID)                        \
	    --env AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY)                \
	    --volume $(PWD):/mnt/workdir                                        \
	    --volume hello-blambda-deps:/root                                   \
	    registry.gitlab.com/tvaughan/hello-blambda:latest                   \
	    bash --login                                                        \
	    #

.PHONY: write-terraform-configs
write-terraform-configs: build-container-image
	@podman run --rm                                                        \
	    --env AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID)                        \
	    --env AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY)                \
	    --volume $(PWD):/mnt/workdir                                        \
	    --volume hello-blambda-deps:/root                                   \
	    registry.gitlab.com/tvaughan/hello-blambda:latest                   \
	    bb blambda terraform write-config                                   \
	    #

.PHONY: build-container-image-lambda
build-lambda: build-container-image is-defined-AWS_ACCESS_KEY_ID is-defined-AWS_SECRET_ACCESS_KEY
	@podman run --rm                                                        \
	    --env AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID)                        \
	    --env AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY)                \
	    --volume $(PWD):/mnt/workdir                                        \
	    --volume hello-blambda-deps:/root                                   \
	    registry.gitlab.com/tvaughan/hello-blambda:latest                   \
	    bb blambda build-all                                                \
	    #

.PHONY: create-remote-state-bucket
create-remote-state-bucket: build-container-image is-defined-AWS_ACCESS_KEY_ID is-defined-AWS_SECRET_ACCESS_KEY
	@podman run --rm                                                        \
	    --env AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID)                        \
	    --env AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY)                \
	    --volume $(PWD):/mnt/workdir                                        \
	    --volume hello-blambda-deps:/root                                   \
	    registry.gitlab.com/tvaughan/hello-blambda:latest                   \
	    aws s3 mb s3://hello-blambda-remote-state                           \
	    #

.PHONY: deploy-lambda
deploy-lambda: is-repo-clean build-lambda create-remote-state-bucket
	@podman run --rm -it                                                    \
	    --env AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID)                        \
	    --env AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY)                \
	    --volume $(PWD):/mnt/workdir                                        \
	    --volume hello-blambda-deps:/root                                   \
	    registry.gitlab.com/tvaughan/hello-blambda:latest                   \
	    bb blambda terraform apply                                          \
	    #

.PHONY: upload-input
upload-input: build-container-image is-defined-AWS_ACCESS_KEY_ID is-defined-AWS_SECRET_ACCESS_KEY
	@podman run --rm                                                        \
	    --env AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID)                        \
	    --env AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY)                \
	    --volume $(PWD):/mnt/workdir                                        \
	    --volume hello-blambda-deps:/root                                   \
	    registry.gitlab.com/tvaughan/hello-blambda:latest                   \
	    aws s3 cp resources s3://hello-blambda/                             \
	    --recursive --exclude "*" --include "*.in.json"                     \
	    #

.PHONY: download-output
download-output: build-container-image is-defined-AWS_ACCESS_KEY_ID is-defined-AWS_SECRET_ACCESS_KEY
	@podman run --rm                                                        \
	    --env AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID)                        \
	    --env AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY)                \
	    --volume $(PWD):/mnt/workdir                                        \
	    --volume hello-blambda-deps:/root                                   \
	    registry.gitlab.com/tvaughan/hello-blambda:latest                   \
	    aws s3 cp s3://hello-blambda/ resources                             \
	    --recursive --exclude "*" --include "*.out.json"                    \
	    #

.PHONY: test-lambda
test-lambda: upload-input download-output

.PHONY: show-logs
show-logs: build-container-image is-defined-AWS_ACCESS_KEY_ID is-defined-AWS_SECRET_ACCESS_KEY
	@podman run --rm                                                        \
	    --env AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID)                        \
	    --env AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY)                \
	    --volume $(PWD):/mnt/workdir                                        \
	    --volume hello-blambda-deps:/root                                   \
	    registry.gitlab.com/tvaughan/hello-blambda:latest                   \
	    aws logs filter-log-events                                          \
	    --log-group-name "/aws/lambda/hello-blambda"                        \
	    #
