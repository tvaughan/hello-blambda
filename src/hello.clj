(ns hello
  (:require
   [cheshire.core :as json]
   [clojure.string :as str]
   [com.grzm.awyeah.client.api :as aws]))

(defonce ^:private s3-client (aws/client {:api :s3}))

(defn- read!
  "Given `bucket` and `key`, return contents as string from S3."
  [bucket key]
  (-> (aws/invoke s3-client {:op :GetObject
                             :request {:Bucket bucket :Key key}})
      :Body
      slurp))

(defn- read-json!
  "Given `bucket` and `key`, return contents as json from S3."
  [bucket key]
  (json/parse-string (read! bucket key)))

(defonce ^:private ip-addr-v4-pattern
  (re-pattern "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$"))

(defn- is-ip-addr-v4?
  "Given string `s`, return true if s is IPv4 addr else return false."
  [s]
  (when (string? s) (boolean (re-matches ip-addr-v4-pattern s))))

(defn find-ip-addrs-v4
  "Given a Cloure data structure or primitive `data`, return a list of IPv4
  addrs found in data."
  [data]
  (cond
    (coll? data) (filter string? (flatten (map find-ip-addrs-v4 data)))
    (string? data) (when (is-ip-addr-v4? data) [data])))

(defn- write!
  "Given `bucket`, `key`, and string `s`, write s as bytes to S3."
  [bucket key s]
      (aws/invoke s3-client {:op :PutObject
                             :request {:Bucket bucket :Key key
                                       :Body (.getBytes s)}}))

(defn- write-json!
  "Given `bucket`, `key`, and a Clojure data structure or private `data`, write
  data as json to S3."
  [bucket key data]
  (write! bucket key (json/generate-string data)))

(defn handler
  [event _]
  (let [bucket (get-in event [:Records 0 :s3 :bucket :name])
        key (get-in event [:Records 0 :s3 :object :key])
        ip-addrs-v4 (find-ip-addrs-v4 (read-json! bucket key))]
    (write-json! bucket (str/replace key #"\.in\.json$" ".out.json") ip-addrs-v4)))
